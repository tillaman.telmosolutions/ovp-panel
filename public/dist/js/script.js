const folders = document.querySelectorAll(".folder");
const passwordInput = document.getElementById("password-input");

folders.forEach((folder) => {
    folder.addEventListener("click", () => {
        folder.classList.toggle("active");
        folder.nextElementSibling.classList.toggle("active");
    });
});
function toggleShowPassword() {
    if (passwordInput.type === "password") {
        passwordInput.type = "text";
    } else {
        passwordInput.type = "password";
    }
}

function generatePassword() {
    var randomstring = Math.random().toString(36).slice(-8);
    passwordInput.value = randomstring;
}


async function selectFile(url,id){
    const res = await fetch(url+id)
    const content = await res.json()
    const ul = document.querySelector(".file-content-result")
    ul.innerHTML = content
}

async function getFiles(url) {
    const res = await fetch(url)
    console.log(await res.json())
}

