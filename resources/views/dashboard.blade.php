<x-app-layout title="Disk Usage">
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <?php
                            $disk_path = "/";
                            $disk_total_space = disk_total_space($disk_path);
                            $disk_free_space = disk_free_space($disk_path);
                            $disk_total_usage = $disk_total_space-$disk_free_space;
                            $disk_total_usage_percentage = 0;
                            if($disk_total_space>0){
                                $disk_total_usage_percentage =($disk_total_usage/$disk_total_space)*100;
                            }
                            $path1 = env("PATH1");
                            $path2 = env("PATH2");
                            ?>
                    <?php
                                function dir_size($directory,$path1) {
                                    $size = 0;
                                    if($directory==$path1."/."||$directory==$path1."/.."){
                                        return -1;
                                    }
                                    if(is_dir($directory)) {
                                        try {
                                            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file){
                                                if(is_file($file)){
                                                    $size += $file->getSize();
                                                }
                                            }
                                            return $size;

                                        } catch (\Throwable $th) {
                                            echo $th;
                                            return 0;
                                        }
                                    }
                                    else{
                                        return filesize($directory);
                                    }
                                }
                                function format_size($size) {
                                    if($size==-1){
                                        return "--";
                                    }
                                    $mod = 1024;
                                    $units = explode(' ','B KB MB GB TB PB');
                                    for ($i = 0; $size > $mod; $i++) {
                                        $size /= $mod;
                                    }
                                    return round($size, 2) . ' ' . $units[$i];
                                }

                                function listFolderFiles($dir,$path1,$withPercentage=false){
                                    $fileFolderList = scandir($dir);
                                    if(!$withPercentage){
                                        echo '<ul class="folder-target">';
                                        foreach($fileFolderList as $fileFolder){
                                            if($fileFolder != '.' && $fileFolder != '..'){
                                                if(!is_dir($dir.'/'.$fileFolder)){
                                                    echo '<li class="file"><span class="file-name">'.$fileFolder.'</span><span class="file-size">'.format_size(dir_size($dir.'/'.$fileFolder,$path1)).'</span>';
                                                } else {
                                                    echo '<li><div class="folder"><span class="folder-name">'.$fileFolder.'</span><span class="folder-size">'.format_size(dir_size($dir.'/'.$fileFolder,$path1)).'</span></div>';
                                                }
                                                if(is_dir($dir.'/'.$fileFolder)) 
                                                {
                                                    listFolderFiles($dir.'/'.$fileFolder,$path1,$withPercentage);
                                                }
                                                echo '</li>';
                                            }
                                        }
                                        echo '</ul>';
                                    }
                                    else{
                                        $total_size = dir_size($path1,$path1);
                                        echo '<ul class="folder-target">';
                                        foreach($fileFolderList as $fileFolder){
                                            if($fileFolder != '.' && $fileFolder != '..'){
                                                $size = dir_size($dir.'/'.$fileFolder,$path1);
                                                $percentage = 0;
                                                if($total_size>0){
                                                    $percentage = ($size/$total_size)*100;
                                                }

                                                    echo 
                                                    '<li class="file">
                                                        <span class="file-name">'
                                                        .$fileFolder.'
                                                        </span>
                                                        <span class="bar-container">
                                                            <span style="width:'.$percentage.'%;" class="bar">
                                                            </span>
                                                        </span>
                                                    </li>';
                                            }
                                        }
                                        echo '</ul>';
                                    }
                                    
                                }
                            ?>

                    <div class="small-box bg-pink">
                        <div class="inner">
                            <h3><?php echo format_size($disk_total_space); ?></h3>
                            <p>Disk Total Space</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>

                    </div>
                </div>

                <div class="col-lg-3 col-6">

                    <div class="small-box bg-pink">
                        <div class="inner">
                            <h3><?php echo format_size($disk_free_space); ?><sup style="font-size: 20px"></sup>
                            </h3>
                            <p>Disk Free Space</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-6">

                    <div class="small-box bg-pink">
                        <div class="inner">
                            <h3><?php echo format_size($disk_total_usage); ?></h3>
                            <p>Disk Total Usage</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-6">

                    <div class="small-box bg-pink   ">
                        <div class="inner">
                            <h3><?php echo number_format($disk_total_usage_percentage,2); ?>%</h3>
                            <p>Disk Usage %</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <?php
                                        $total_size = dir_size($path1,$path1);
                                    ?>
                        <div class=" card-header ui-sortable-handle">
                            <div class="d-flex justify-content-between">
                                <h3 class=" card-title font-weight-bold">Server Details</h3>
                            </div>
                        </div>
                        <div class="card-body px-0">
                            <?php
                                        echo '<ul class="directories">';
                                        echo 
                                        '
                                        <li class="file">
                                            <span class="file-name">
                                                SERVER NAME
                                            </span>
                                            <span class="file-size">
                                                '.
                                            $_SERVER['SERVER_NAME']
                                                .'
                                            </span>
                                        </li>

                                        <li class="file">
                                            <span class="file-name">
                                                HOST NAME
                                            </span>
                                            <span class="file-size">
                                                '.
                                               gethostname()
                                                .'
                                            </span>
                                        </li>

                                        <li class="file">
                                            <span class="file-name">
                                                FILE NAME
                                            </span>
                                            <span class="file-size">
                                                '.
                                                $_SERVER['PHP_SELF']
                                                .'
                                            </span>
                                        </li>

                                        
                                        <li class="file">
                                        <span class="file-name">
                                           HTTP HOST
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['HTTP_HOST']
                                            .'
                                        </span>

                                        <li class="file">
                                        <span class="file-name">
                                           HTTP USER AGENT
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['HTTP_USER_AGENT']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          SCRIPT NAME
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['SCRIPT_NAME']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          SERVER SOFTWARE
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['SERVER_SOFTWARE']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          SERVER PORT
                                        </span>
                                        <span class="file-size">
                                            '.
                                        $_SERVER['SERVER_PORT']
                                            .'
                                        </span>
                                        </li>

                                        <li class="file">
                                        <span class="file-name">
                                          Memory usage
                                        </span>
                                        <span class="file-size">
                                            '.
                                            format_size(memory_get_usage())
                                            .'
                                        </span>
                                        </li>

                                        
                                        
                                        
                                        ';
                                        echo '</ul>';
                                    ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <?php
                                        $total_size = dir_size($path1,$path1);
                                    ?>
                        <div class="card-header ui-sortable-handle">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title font-weight-bold">Database Information</h3>
                            </div>
                        </div>
                        <div class="card-body px-0">

                            <?php
                            $mysqli = new mysqli(env("DB_HOST"), env("DB_USERNAME"), env("DB_PASSWORD"),env("DB_DATABASE"));
                            ?>
                            <?php
                                        echo '<ul class="directories">';
                                        echo 
                                        '
                                        <li class="file">
                                        <span class="file-name">
                                          Client Info
                                        </span>
                                        <span class="file-size">
                                            '.
                                            mysqli_get_client_info()
                                            .'
                                        </span>
                                        </li>
                                        <li class="file">
                                        <span class="file-name">
                                          Client Version
                                        </span>
                                        <span class="file-size">
                                            '.
                                            mysqli_get_client_version()
                                            .'
                                        </span>
                                        </li>
                                        <li class="file">
                                        <span class="file-name">
                                        Server Info
                                        </span>
                                        <span class="file-size">
                                            '.
                                            mysqli_get_server_info($mysqli)
                                            .'
                                        </span>
                                        </li>
                                        <li class="file">
                                        <span class="file-name">
                                          Host Info
                                        </span>
                                        <span class="file-size">
                                            '.
                                            mysqli_get_host_info($mysqli)
                                            .'
                                        </span>
                                        </li>
                                        <li class="file">
                                        <span class="file-name">
                                          Database Usage
                                        </span>
                                        <span class="file-size">
                                            '.
                                           $db_size
                                            .' MB
                                        </span>
                                        </li>
                                        ';
                                        echo '</ul>';
                                    ?>
                        </div>
                    </div>
                </div>
            </div> -->


            <div class="row">
                <div class="col-lg-6">
                    <div class="card ">
                        <?php
                                       
                                        $total_size = dir_size($path2,$path2);
                                    ?>
                        <div class="card-header ui-sortable-handle">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title font-weight-bold">Total Size</h3>
                                <h3 class="card-title"><?php echo format_size($total_size);?></h3>
                            </div>
                        </div>
                        <div class="card-body px-0 ">
                            <?php
                                       
                                        echo '<ul class="directories">';
                                        listFolderFiles($path2,$path2,true);
                                        echo '</ul>';

                                    ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <?php
                            $total_size = dir_size($path1,$path1);
                        ?>
                        <div class="card-header ui-sortable-handle">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title font-weight-bold">Total Size</h3>
                                <h3 class="card-title">
                                    <?php echo format_size($total_size);?></h6>
                            </div>
                        </div>
                        <div class="card-body px-0">
                            <?php
                                echo '<ul class="directories">';
                                listFolderFiles($path1,$path1);
                                echo '</ul>';

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>