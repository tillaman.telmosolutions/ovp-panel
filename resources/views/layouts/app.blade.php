<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>{{ config('app.name', 'OVP Panel') }}</title>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
    <link rel="stylesheet" href="{{ asset('/dist/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('/plugins/fontawesome-free/css/all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/dist/css/adminlte.min.css') }}" />

    @yield('styles')
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Styles -->
    @livewireStyles
    <!-- Scripts -->

    <script src="{{ asset('/js/app.js') }}" defer></script>

</head>

<body class="hold-transition sidebar-mini">
    @props(['title' => 'OVP Panel','active'=>'disk-usage'])
    <div class="wrapper ">
        <!-- Navbar -->
        <nav  class="border-bottom-0 main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside style="background-color:#d63384" class=" main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="border-white brand-link text-white">
                <span class=" brand-text font-weight-bold">OVP ADMIN</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item {{$active=='disk-usage'?'menu-open':''}}">
                            <a href="{{ route('ovp_panel.dashboard') }}"
                                class="text-white nav-link {{$active=='disk-usage'?'active bg-white':''}}">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>Disk Usage</p>
                            </a>
                        </li>
                        <li class="nav-item {{$active=='view-users'?'menu-open menu-is-opening':''}}">
                            <a href="{{ route('ovp_panel.dashboard.users') }}"
                                class="text-white nav-link {{$active=='view-users'||$active=='create-user'?'active bg-white':''}}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Manage Users
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="display: {{$active=='view-users'||$active=='create-user'?'block':'none'}};">
                                <li class="nav-item">
                                    <a href="{{ route('ovp_panel.dashboard.users') }}"
                                        class="nav-link {{$active=='view-users'?'active text-black':'text-white '}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>View Users</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('ovp_panel.dashboard.users.create') }}"
                                        class=" nav-link {{$active=='create-user'?'active text-black':'text-white'}}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Create User</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        
                       

                        <li class="nav-item ">
                            <a href="{{ route('ovp_panel.dashboard.users') }}"
                                class="text-white nav-link ">
                                <i class="nav-icon fas fa-server"></i>
                                <p>
                                   Servers
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                >
                                <li class="nav-item">
                                    <a target="_blank" href="https://ovp.gov.ph/ovpadmin"
                                        class="nav-link text-white">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>OVP</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a target="_blank" href="https://rs1.ovp.gov.ph/ovpadmin"
                                        class="nav-link text-white">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>RS1</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a target="_blank" href="https://rs2.ovp.gov.ph/ovpadmin"
                                        class="nav-link text-white">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>RS2</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a target="_blank" href="https://staging.ovp.gov.ph/ovpadmin"
                                        class="nav-link text-white">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Staging</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a target="_blank" href="https://sandbox.ovp.gov.ph/ovpadmin"
                                        class="nav-link text-white">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Sandbox</p>
                                    </a>
                                </li>
                                
                            </ul>
                        </li>
                        <li class="nav-item {{$active=='logs'?'menu-open':''}}">
                            <a href=" {{ route('ovp_panel.dashboard.logs') }}"
                                class="text-white nav-link {{$active=='logs'?'active bg-white':''}}">
                                <i class="nav-icon fas fa-file"></i>
                                <p>
                                    Logs
                                </p>
                            </a>
                        </li>

                        <li class="nav-item {{$active=='settings'?'menu-open':''}}">
                            <a href=" {{ route('ovp_panel.dashboard.settings') }}"
                                class="text-white nav-link {{$active=='settings'?'active bg-white':''}}">
                                <i class="nav-icon fas fa-wrench"></i>
                                <p>
                                    Settings
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <form method="POST" action="{{ route('logout') }}" x-data>
                                @csrf
                                <a onclick="event.preventDefault(); this.closest('form').submit();"
                                    href="{{ route('logout') }}" class="text-white nav-link">
                                    <i class="nav-icon fas fa-key"></i>
                                    <p>
                                        Logout
                                    </p>
                                </a>
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">{{$title}}</h1>
                        </div>
                    </div>
                </div>
            </div>
            {{ $slot }}
        </div>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline"></div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2022
                <a class="text-pink" href="https://ovp.gov.ph">OVP Panel</a>.</strong>
            All rights reserved.
        </footer>
    </div>

    @stack('modals')

    @livewireScripts

    <script src="{{ asset('/dist/js/script.js') }}"></script>
    <script src="{{ asset('/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>
</body>


</html>