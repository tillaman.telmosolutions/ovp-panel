
 <?php
                            $path1 = env("LOGPATH");
                            $path2 = env("LOGPATH");
                            ?>
                    <?php
                                function dir_size($directory,$path1) {
                                    $size = 0;
                                    if($directory==$path1."/."||$directory==$path1."/.."){
                                        return -1;
                                    }
                                    if(is_dir($directory)) {
                                        try {
                                            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file){
                                                if(is_file($file)){
                                                    $size += $file->getSize();
                                                }
                                            }
                                            return $size;

                                        } catch (\Throwable $th) {
                                            echo $th;
                                            return 0;
                                        }
                                    }
                                    else{
                                        return filesize($directory);
                                    }
                                }
                                function format_size($size) {
                                    if($size==-1){
                                        return "--";
                                    }
                                    $mod = 1024;
                                    $units = explode(' ','B KB MB GB TB PB');
                                    for ($i = 0; $size > $mod; $i++) {
                                        $size /= $mod;
                                    }
                                    return round($size, 2) . ' ' . $units[$i];
                                }

                                function listFolderFiles($dir,$path1,$withPercentage=false){
                                    $ignored = [];
                                    $files = array();
                                    foreach (scandir($dir) as $file) {
                                        if (in_array($file, $ignored)) continue;
                                        $files[$file] = filemtime($dir . '/' . $file);
                                    }
                                    arsort($files);
                                    $files = array_keys($files);

                                    $fileFolderList = $files;
                                    // $fileFolderList = scandir($dir);
                                    if(!$withPercentage){
                                        echo '<ul class="folder-target">';
                                        foreach($fileFolderList as $key=>$fileFolder){
                                            if($fileFolder != '.' && $fileFolder != '..'){
                                                if(!is_dir($dir.'/'.$fileFolder)){
                                                    $logs_api = env("LOGS_API")
                                                    ?>
                                                   <li onclick="selectFile('<?php echo $logs_api?>','<?php echo $key ?>')" class="file-log"><span class="file-name"><?php echo $fileFolder ?></span>
                                                    <?php
                                                } 
                                                echo '</li>';
                                            }
                                        }
                                        echo '</ul>';
                                    }
                                    else{
                                        $total_size = dir_size($path1,$path1);
                                        echo '<ul class="folder-target">';
                                        foreach($fileFolderList as $fileFolder){
                                            if($fileFolder != '.' && $fileFolder != '..'){
                                                $size = dir_size($dir.'/'.$fileFolder,$path1);
                                                $percentage = 0;
                                                if($total_size>0){
                                                    $percentage = ($size/$total_size)*100;
                                                }

                                                    echo 
                                                    '<li class="file-log">
                                                        <span class="file-name">'
                                                        .$fileFolder.'
                                                        </span>
                                                        <span class="bar-container">
                                                            <span style="width:'.$percentage.'%;" class="bar">
                                                            </span>
                                                        </span>
                                                    </li>';
                                            }
                                        }
                                        echo '</ul>';
                                    }
                                    
                                }
                            ?>
<x-app-layout active="logs" title="Logs">
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                        <?php
                            $total_size = dir_size($path1,$path1);
                        ?>
                    <div class="card-header ui-sortable-handle">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title font-weight-bold">Files</h3>
                           
                        </div>
                    </div>
                    <div class="card-body px-0">
                        <?php
                            echo '<ul class="directories">';
                            listFolderFiles($path1,$path1);
                            echo '</ul>';

                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
            <div class="card">
                <div class="card-header ui-sortable-handle">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title font-weight-bold">Content</h3>
                    </div>
                </div>
                <div class="card-body px-0">
  
                    <ul class="file-content-result">
                                Please select a file to view its content.
                    </ul>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</x-app-layout>