<x-app-layout title="Edit User" active="edit-user">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header bg-pink">
                            <h3 class="card-title">User Information</h3>
                        </div>
                        <form method="POST" action="{{ route('ovp_panel.dashboard.users.update',['user'=>$user->id]) }}">
                            @csrf
                            @method('PATCH')
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input value="{{$user->name}}" name="name" type="text" class="form-control"
                                        id="name" placeholder="Enter name">
                                </div>
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input value="{{$user->email}}" name="email" type="email" class="form-control"
                                        id="email" placeholder="Enter email">
                                </div>
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input name="password" type="password" class="form-control"
                                                id="password-input" placeholder="Password">
                                        </div>
                                        <div class="input-group-append">
                                            <span onclick="generatePassword()"
                                                class="pointer input-group-text">Generate</span>
                                            <span onclick="toggleShowPassword()" id="show-password-btn"
                                                class="pointer input-group-text">Show</span>
                                        </div>
                                    </div>
                                </div>
                                @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-default ">Submit</button>
                            </div>
                        </form>
                    </div>


                </div>

            </div>
        </div>
    </section>
</x-app-layout>