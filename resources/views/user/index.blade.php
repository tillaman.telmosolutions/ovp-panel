<x-app-layout title="Manage Users" active="view-users">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-pink">
                            <h3 class="card-title">Users</h3>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>User</th>
                                        <th>Date Created</th>
                                        <th>Email</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->created_at}}</td>
                                        <td>{{$user->email}}</td>
                                        <td><a href="{{ route('ovp_panel.dashboard.users.edit',['user'=>$user->id]) }}"
                                                class="badge bg-pink">Edit</a></td>
                                        <td>

                                            <form
                                                action="{{ route('ovp_panel.dashboard.users.destroy',['user'=>$user->id]) }}"
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="border-0 badge bg-danger">Delete</button>
                                        </td>
                                        </form>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
</x-app-layout>