<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-logs', function () {
    $dir = getenv("LOGPATH");
    $fileFolderList = scandir($dir);
    unset($fileFolderList[0]);
    unset($fileFolderList[1]);
    return response()->json($fileFolderList);
})->name("ovp_panel.dashboard.get-logs");


Route::get('get-logs/{id}', function ($id) {
    
    try {
        $dir = getenv("LOGPATH");
        $file = scandir($dir)[$id];
        $file_content = file_get_contents($dir."./".$file);
        return response()->json($file_content);
    } catch (\Throwable $th) {
        return response()->json($th.message);
    }
   
})->name("ovp_panel.dashboard.get-logs.id");
