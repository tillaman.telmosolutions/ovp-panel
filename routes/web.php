<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\UserController;


Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/', function () {
        $result = DB::select(DB::raw('SELECT table_name AS "Table",
        ((data_length + index_length) / 1024/1024) AS "Size"
        FROM information_schema.TABLES
        WHERE table_schema ="'.getenv("DB_DATABASE"). '"
        ORDER BY (data_length + index_length) DESC'));
        $size = array_sum(array_column($result, 'Size'));
        $db_size = number_format((float)$size, 2, '.', '');
        
        return view('dashboard',[
            'db_size'=>$db_size
        ]);
    })->name("ovp_panel.dashboard");
    Route::get('settings', function () {
        return view('settings');
    })->name("ovp_panel.dashboard.settings");

    Route::get('logs', function () {
        return view('logs');
    })->name("ovp_panel.dashboard.logs");
    Route::resource('users', UserController::class,[
        'names'=>[
            'index'=>"ovp_panel.dashboard.users",
            'create'=>"ovp_panel.dashboard.users.create",
            'store'=>"ovp_panel.dashboard.users.store",
            'show'=>"ovp_panel.dashboard.users.show",
            'edit'=>"ovp_panel.dashboard.users.edit",
            'update'=>"ovp_panel.dashboard.users.update",
            'destroy'=>"ovp_panel.dashboard.users.destroy",
        ]
    ]);

});